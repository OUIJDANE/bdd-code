import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import com.sdz.connection.ConnectionToBDD;

public class JTable_Search extends JFrame {

  /**
  * ToolBar pour le lancement des requtes
  */
  private JToolBar tool = new JToolBar();
  private JToolBar tool1 = new JToolBar();

  /**
  * Le bouton
  */
  private JButton load = new javax.swing.JButton("           Search       ");
  private JButton load1 = new javax.swing.JButton("            Back       ");
  //fenetre.setLayout(new GridLayout(3, 2));
  
  /**
  * Le délimiteur
  */
  private JSplitPane split;

  /**
  * Le conteneur de résultat
  */
  private JPanel result = new JPanel();

  /**
  * Requete par défaut pour le démarrage
  */
  private String requete1 = "";
  private String requete = "SELECT  * FROM personne WHERE nom='"+requete1+"'";
  /**
  * Le composant dans lequel on tape la requete
  */
  private JTextArea text = new JTextArea(requete1);
  //text.setPreferredSize(new Dimension(150, 30));
		
  /**
  * Constructeur
  */
  public Fenetre(){
    setSize(900, 600);
    setTitle("JDBC");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
		
    initToolbar();
    initToolbar1();
    initContent();
    initTable("SELECT  * FROM personne");
  }
	
  /**
  * Initialiser la toolbar
  */
  private void initToolbar(){
    load.setPreferredSize(new Dimension(2, 4));
    load.setBorder(null);
    load.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent event){
        initTable("SELECT  * FROM personne WHERE nom='"+text.getText()+"'");
      }
    });
    tool.add(load);
    BorderLayout bl=new BorderLayout(47,4);
    getContentPane().add(tool, BorderLayout.NORTH);
  }
  private void initToolbar1(){
      load1.setPreferredSize(new Dimension(40, 40));
      load1.setBorder(null);
      load1.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent event){
          initTable("SELECT  * FROM personne");
        }
      });
      tool.add(load1);
      BorderLayout bl=new BorderLayout(4,5);
      getContentPane().add(tool, BorderLayout.NORTH);
  }
	
  /**
  * Initialiser le contenu de la fenetre
  */
  public void initContent(){
    //Vous connaissez a...
    result.setLayout(new BorderLayout());
    split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(text), result);
    split.setDividerLocation(100);
    getContentPane().add(split, BorderLayout.CENTER);		
  }
	
  /**
  * Initialiser le visuel avec la requete saisie dans l'diteur
  * @param query
  */
  public void initTable(String query){
    try {
    //On cre un statement
      long start = System.currentTimeMillis();
      Statement state = Connect.getInstance().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

      //On excute la requte
      ResultSet res = state.executeQuery(query);
      //Temps d'excution

      //On rcupre les meta afin de rcuprer le nom des colonnes
      ResultSetMetaData meta = res.getMetaData();
      //On initialise un tableau d'Object pour les en-ttes du tableau
      Object[] column = new Object[meta.getColumnCount()];

      for(int i = 1 ; i <= meta.getColumnCount(); i++)
        column[i-1] = meta.getColumnName(i);

      //Petite manipulation pour obtenir le nombre de lignes
      res.last();
      int rowCount = res.getRow();
      Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

      //On revient au dpart
      res.beforeFirst();
      int j = 1;

      //On remplit le tableau d'Object[][]
      while(res.next()){
        for(int i = 1 ; i <= meta.getColumnCount(); i++)
          data[j-1][i-1] = res.getObject(i);
				
        j++;
      }

      //On ferme le tout                                     
      res.close();
      state.close();

      long totalTime = System.currentTimeMillis() - start;

      //On enlève le contenu de notre conteneur
      result.removeAll();
      //On y ajoute un JTable
      result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
      result.add(new JLabel("La requte  t excuter en " + totalTime + " ms et a retourn " + rowCount + " ligne(s)"), BorderLayout.SOUTH);
      //On force la mise  jour de l'affichage
      result.revalidate();
			
    } catch (SQLException e) {
      //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
      result.removeAll();
      result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
      result.revalidate();
      JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	
  }

  /**
  * Point de départ du programme
  * @param args
  */
  public static void main(String[] args){
    Fenetre fen = new Fenetre();
  fen.setVisible(true);
  }
}