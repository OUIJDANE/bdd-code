------------------------------------------------------------------
-- Suppression de la relation si elle est déja créée
-- (enlever les tirets de commentaires)
------------------------------------------------------------------

--DROP TABLE Personne;

------------------------------------------------------------------
-- Script de création de la table Personne
------------------------------------------------------------------
CREATE TABLE Personne
(
 Nom        	   varchar(25) NOT NULL,
 Prenom            varchar(25) NOT NULL,
 Age             varchar(25) NOT NULL, 
 CONSTRAINT PK_Personne PRIMARY KEY (Nom) -- Définition de la clé primaire
 );

